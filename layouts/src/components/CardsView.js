import react from "react";
import ShopCard from "./ShopCard";

const CardsView = (props) =>{

    const {cards} = props;

    return(
        <>
            {cards.map(card => <ShopCard card={card}/>)}
        </>
    );

};

export default CardsView;