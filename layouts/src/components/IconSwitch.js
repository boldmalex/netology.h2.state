import react from "react";
import "./IconSwitch.css";


const IconSwitch = (props) =>{

    const {icon, onSwitch} = props;

    console.log(icon);

    return(
      <div>
        <button className="material-icons" onClick={onSwitch}>
          {icon}
        </button>
      </div>  
    );

};

export default IconSwitch;