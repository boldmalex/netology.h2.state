import react from "react";
import ShopItem from "./ShopItem";

const ListView = (props) =>{

    const {items} = props;

    return(
        <div>
            {items.map(item => <ShopItem item={item}/>)}
        </div>
    );

};

export default ListView;