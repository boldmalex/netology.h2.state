import react from "react";
import "./ShopCard.css";


const ShopCard = (props) =>{

    const {card} = props;

    return(
        <div className="shopCard">
            <h2>{card.name}</h2>
            <h5>{card.color}</h5>
            <img src={card.img} alt={card.img}></img>
            <h4>{card.price}</h4>
            <button>Add to cart</button>
        </div>
    );
};

export default ShopCard;