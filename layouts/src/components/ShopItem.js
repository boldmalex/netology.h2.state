import react from "react";
import "./ShopItem.css";


const ShopItem = (props) =>{

    const {item} = props;

    return(
        <div>
            <table className="fixed">
                <col width="20%"></col>
                <col width="30%"></col>
                <col width="10%"></col>
                <col width="20%"></col>
                <col width="20%"></col>
                <tr>
                    <td><img src={item.img} alt={item.img}></img></td>
                    <td><h2>{item.name}</h2></td>
                    <td><h5>{item.color}</h5></td>
                    <td><h4>{item.price}</h4></td>
                    <td><button>Add to cart</button></td>
                </tr>
            </table>
        </div>
    );

};

export default ShopItem;