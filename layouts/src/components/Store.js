import react, {useState} from "react";
import CardsView from "./CardsView";
import IconSwitch from "./IconSwitch";
import ListView from "./ListView";

const Store = (props) =>{

    const {products} = props;


    const [icon, setIcon] = useState("view_module");

    const onIconSwitch = () =>{
        setIcon(prevIcon => prevIcon === "view_module" ? "view_list" : "view_module");
    };


    const showStore = (isCardsShow) =>{
        if (isCardsShow)
            return <CardsView cards={products}/>
        else
            return <ListView items={products}/>
    }


    return(
        <div>
            <IconSwitch icon={icon} onSwitch={onIconSwitch} />
            {showStore(icon === "view_module")}
        </div>
    );
    
};

export default Store;