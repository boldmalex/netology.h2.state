import react, { useState } from "react";
import Toolbar from "./toolbar.js";
import ProjectList from "./project-list.js";


const Portfolio = (props) =>{

    const {items: inputItems} = props;

    const filters = ["All", "Websites", "Flayers", "Business Cards"];


    const [items, setItems] = useState(inputItems);

    const [selected, setSelected] = useState("All");


    const onSelectFilter = (filter) =>{
        setItems(inputItems.filter(item => filter === "All" || item.category === filter));
        setSelected(filter);
    };



    return(
        <div>
            <Toolbar 
                filters = {filters} 
                selected = {selected} 
                onSelectFilter = {onSelectFilter}/>

            <ProjectList items={items}/>
        </div>
    );

}

export default Portfolio;