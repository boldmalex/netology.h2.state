import react from "react";
import ToolbarButton from "./toolbar-button.js";
import PropTypes from "prop-types";


const Toolbar = (props) =>{

    const {filters = [], selected, onSelectFilter} = props;

    if (filters.length > 0)
    {
        return(
            <div>
                {filters.map(filter => <ToolbarButton 
                                        key={filter} 
                                        text={filter} 
                                        selected={selected}
                                        onClick={onSelectFilter}/>)}
            </div>
        );
    }
    else
        return null;

};


Toolbar.propTypes = {
    filters: PropTypes.arrayOf(PropTypes.string).isRequired,
    selected: PropTypes.string.isRequired,
    onSelectFilter: PropTypes.func.isRequired
};


export default Toolbar;