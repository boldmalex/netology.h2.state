import react from "react";
import PropTypes from "prop-types";
import { object } from "prop-types";


const ProjectList = (props) =>{

    const {items=[]} = props;

    if (items.length > 0)
    {
        return(
            <div>
                {items.map(item => <img key={item.id} src={item.img} alt={item.img}></img>)}
            </div>
        );
    }
    else
        return null;
}


ProjectList.propTypes = {
    items: PropTypes.arrayOf(object).isRequired
}

export default ProjectList;