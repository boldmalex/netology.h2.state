import react from "react";
import "./toolbar-button.css";



const ToolbarButton = (props) =>{

    const {text, selected, onClick} = props;

    const onButtonClick = () =>{
        onClick(text);
    };

    return(
        <>
            <button onClick={onButtonClick}
                    className={selected === text ? "button-selected" : "button-unselected"}>
                {text}
            </button>
        </>
    );

};


export default ToolbarButton;